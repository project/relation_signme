<?php

/**
 * @file
 * Provide views handlers and plugins that allow usage of PHP.
 */

/**
 * Implements hook_views_data().
 */
function relation_signme_views_data() {
  $data['global']['relation_signme'] = array(
    'title' => t('Relation Sign me'),
    'help' => t('Connect entity with current user.'),
    'area' => array(
      'help' => t('Connect entity related by view argument with current user.'),
      'handler' => 'relation_signme_handler_area',
    ),
    'field' => array(
      'help' => t('Connect entity related by view argument or listed item with current user.'),
      'handler' => 'relation_signme_handler_field',
    ),
  );
  dpm('relation_signme_views_data()');
  return $data;
}

